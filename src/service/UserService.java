package service;

import domain.Person;
import domain.Role;
import domain.User;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
            return users.stream().filter((User c) -> c.getPersonDetails().getAddresses().size() > 1  )
                                    .collect(Collectors.toList());

    }

//    public static Person findOldestPerson(List<User> users) {
        
  /*  }

    public static User findUserWithLongestUsername(List<User> users) {
        
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
        
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
        
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
        
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
        
    }*/
}
