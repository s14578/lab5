package main;
import service.UserService;
import domain.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {


    private static List<User> userList;
    // public Person(String name, String surname, List<String> phoneNumbers, List<Address> addresses, Role role, int age)
    // public Address(String streetName, Integer houseNumber, Integer flatNumber, String city, String postCode, String country) {
    private static void personList() {

        Address address1 = new Address("Sloneczna",16,0,"Goscicino", "84-241","Poland");
        Address address2 = new Address("Slonecznaa",17,1,"Goscicinoo", "84-2411","Polandd");
        Address address3 = new Address("Slonecznaaa",18,2,"Goscicinooo", "84-24111","Polanddd");
        List<Address> addressList1 = Arrays.asList(address1,address2,address3);
        List<String> phonenumberList1 = Arrays.asList("13123131","124141241");
        List<Permission> permissions1 = Arrays.asList(new Permission("Leon"), new Permission("Cos"));
        Role role1 = new Role("Pracownik",permissions1);
        Person person1 = new Person("Jan","Kowalski",phonenumberList1,addressList1,role1, 79 );
            User user1 = new User("SDADA","klops",person1);

        Person person2 = new Person("Janek","Kowalskowy",phonenumberList1,addressList1,role1, 16 );
            User user2 = new User("OKO","nic",person2);

        userList = Arrays.asList(user1, user2);

    }
    public static void main(String[] args) {
        personList();
        List<User> result =  UserService.findUsersWhoHaveMoreThanOneAddress(userList);
        System.out.println(result.toString());


    }


}
